/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.squareup.longaike;

import com.squareup.phrase.ListPhrase;
import junit.framework.Assert;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.AbstractList;
import java.util.Collections;
import java.util.List;

import static com.squareup.phrase.ListPhrase.from;
import static java.util.Arrays.asList;

public class ListPhraseTest extends TestCase {
    private final static String twoElementSeparator = " and ";
    private final static String nonFinalElementSeparator = ", ";
    private final static String finalElementSeparator = ", and ";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    ListPhrase listPhrase;

    @Before
    public void setUp() {
        listPhrase =
                from(twoElementSeparator, nonFinalElementSeparator, finalElementSeparator);
    }

    @Test
    public void testFromNullThrows() {
        try {
            ListPhrase.from(null);
            fail("separator cannot be null");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "separator cannot be null");
        }
    }

    @Test
    public void testFromNullTwoElementThrows() {
        try {
            ListPhrase.from(null, ",", ",");
            fail("two-element separator cannot be null");
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), "two-element separator cannot be null");
        }

    }

    @Test
    public void testFromNullNonFinalElementThrows() {
        try {
            from(",", null, ",");
            fail("non-final separator cannot be null");
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), "non-final separator cannot be null");
        }
    }

    @Test
    public void testFromNullFinalElementThrows() {
        try {
            from(",", ",", null);
            fail("final separator cannot be null");
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), "final separator cannot be null");
        }
    }

    @Test
    public void testJoinEmptyListThrows() {
        try {
            listPhrase.join(Collections.emptyList());
            fail("list cannot be empty");
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), "list cannot be empty");
        }
    }

    @Test
    public void testJoinEmptyStringThrows() {
        try {
            listPhrase.join(asList(""));
            fail("formatted list element cannot be empty at index 0");
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), "formatted list element cannot be empty at index 0");
        }
    }

    @Test
    public void testJoinOneItemReturnsIdentity() {
        org.junit.Assert.assertEquals(listPhrase.join(asList("one")), "one");
    }

    @Test
    public void testJoinTwoItemsUsesTwoElementSeparator() {
        Assert.assertEquals(listPhrase.join(asList("one", "two")), "one and two");
    }

    @Test
    public void testJoinThreeItemsUsesThreeElementSeparator() {
        Assert.assertEquals(listPhrase.join(asList("one", "two", "three")), "one, two, and three");
    }

    @Test
    public void testJoinMoreThanThreeItems() {
        Assert.assertEquals(listPhrase.join(asList("one", "two", "three", "four")), "one, two, three, and four");
    }

    @Test
    public void testJoinNullIterableNullFormatter() {
        try {
            listPhrase.join(null, null);
            fail("list cannot be null");
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), "list cannot be null");
        }
    }

    @Test
    public void testJoinNullIterableNonNullFormatter() {
        try {
            listPhrase.join(null, new ListPhrase.Formatter<Object>() {
                @Override
                public CharSequence format(Object item) {
                    return "hi";
                }
            });
            fail("list cannot be null");
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), "list cannot be null");
        }
    }

    @Test
    public void testJoinNullElementInIterableThrows() {
        try {
            listPhrase.join(asList("foo", null));
            fail("list element cannot be null at index 1");
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), "list element cannot be null at index 1");
        }
    }

    @Test
    public void testJoinNullElementInIterableWithFormatterThrows() {
        try {
            listPhrase.join(asList("foo", null), new ListPhrase.Formatter<String>() {
                @Override
                public CharSequence format(String item) {
                    return "bar";
                }
            });
            fail("list element cannot be null at index 1");
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), "list element cannot be null at index 1");
        }
    }

    @Test
    public void testJoinWithFormatterReturningNullThrows() {
        try {
            listPhrase.join(asList("foo"), new ListPhrase.Formatter<String>() {
                @Override
                public CharSequence format(String item) {
                    return null;
                }
            });
            fail("formatted list element cannot be null at index 0");
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), "formatted list element cannot be null at index 0");
        }
    }

    @Test
    public void testJoinNonCharSequenceUsesToString() {
        Assert.assertEquals(listPhrase.join(asList(1, 2, 3)), "1, 2, and 3");
    }

    @Test
    public void testJoinWithFormatter() {
        ListPhrase.Formatter<Integer> formatter = new ListPhrase.Formatter<Integer>() {
            @Override
            public CharSequence format(Integer item) {
                return String.format("0x%d", item);
            }
        };
        Assert.assertEquals(listPhrase.join(asList(1, 2, 3), formatter), "0x1, 0x2, and 0x3");
    }

    @Test
    public void testJoinReallyLongListDoesntOverflowStack() {
        List<Integer> longList = new AbstractList<Integer>() {
            @Override
            public Integer get(int location) {
                return location;
            }

            @Override
            public int size() {
                return 999_999;
            }
        };

        final CharSequence join = listPhrase.join(longList, new ListPhrase.Formatter<Integer>() {
            @Override
            public CharSequence format(Integer item) {
                // Do as little as possible.
                return "a";
            }
        });
    }
}
