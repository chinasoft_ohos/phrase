/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.squareup.longaike;

import com.squareup.phrase.Phrase;
import junit.framework.Assert;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Text;
import ohos.app.Context;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static com.squareup.phrase.Phrase.from;
import static org.junit.Assert.fail;

public class PhraseTest {


    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testEmptyStringFormatsToItself() {
        Assert.assertEquals(Phrase.from("").format().toString(), "");
    }

    @Test
    public void testTrivialStringFormatsToItself() {
        Assert.assertEquals(Phrase.from("Hello").format().toString(), "Hello");
    }

    @Test
    public void testPuttingNullValueThrowsException() {
        try {
            from("hi {name}").put("name", null);
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "Null value for 'name'");
        }
    }


    @Test
    public void testToStringReturnsThePattern() {
        Assert.assertEquals(from("hello {name}").put("name", "{name}").format().toString(), "hello {name}");
    }

    @Test
    public void testSingleCurlyBraceIsAMistake() {
        try {
            from("{");
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "Unexpected first character '\u0000'; must be lower case a-z.");
        }
    }

    @Test
    public void testtwoLeftCurlyBracesFormatAsSingleCurlyBrace() {
        Assert.assertEquals(Phrase.from("{{").format().toString(), "{");
    }

    @Test
    public void testSimpleSuccessfulSubstitution() {
        Assert.assertEquals(Phrase.from("hi {name}").put("name", "Eric").format().toString(), "hi Eric");
    }

    @Test
    public void testLoneCurlyBraceIsAMistake() {
        try {
            from("hi { {age}.");
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "Unexpected first character ' '; must be lower case a-z.");
        }
    }

    @Test
    public void testIgnoresTokenNextToEscapedBrace() {
        Assert.assertEquals(Phrase.from("hi {{name} {name}").put("name", "Bubba").format().toString(), "hi {name} Bubba");
    }

    @Test
    public void testCanEscapeCurlyBracesImmediatelyBeforeKey() {
        Assert.assertEquals(Phrase.from("you are {{{name}").put("name", "Steve").format().toString(), "you are {Steve");
    }

    @Test
    public void testCanReplaceKeysAndReusePhraseInstance() {
        Phrase pattern = from("hi {name}.");
        Assert.assertEquals(pattern.put("name", "George").format().toString(), "hi George.");
        pattern.put("name", "Abe");
        Assert.assertEquals(pattern.put("name", "Abe").format().toString(), "hi Abe.");
    }

    @Test
    public void testPatternsCanHaveSeveralKeys() {
        Assert.assertEquals(from("hi {name}, you are {age} years old. {name}").put("name", "Abe")
                .put("age", 20)
                .format()
                .toString(), "hi Abe, you are 20 years old. Abe");
    }

    @Test
    public void testPutOptionalIgnoresKey() {
        Assert.assertEquals(from("Hello").putOptional("key", "value").format().toString(), "Hello");
    }

    @Test
    public void testPutOptionalWorksIfKeyIsPresent() {
        Assert.assertEquals(from("Hello {name}").putOptional("name", "Eric").format().toString(), "Hello Eric");
    }

    @Test
    public void testPutFailUppercaseNotAllowedMiddle() {
        try {
            from("Hello {aName}").putOptional("Name", "Eric").format();
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "Unexpected character 'N'; expecting lower case a-z, '_', or '}'");
        }
    }

    private Phrase gender = from("{gender}");

    @Test
    public void testFormatFailsFastWhenKeysAreMissing() {
        try {
            gender.format();
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "Missing keys: [gender]");
        }
    }

    @Test
    public void testPutFailsFastWhenPuttingUnknownKey() {
        try {
            gender.put("bogusKey", "whatever");
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "Invalid key: bogusKey");
        }
    }

    @Test
    public void testEmptyTokenFailsFast() {
        try {
            from("illegal {} pattern");
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "Unexpected first character '}'; must be lower case a-z.");
        }
    }

    @Test
    public void testIllegalStartOfTokenCharactersFailFast() {
        try {
            from("blah {NoUppercaseAllowed}");
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "Unexpected first character 'N'; must be lower case a-z.");
        }
    }

    @Test
    public void testTokensCanHaveUnderscores() {
        Assert.assertEquals(from("{first_name}").put("first_name", "Eric").format().toString(), "Eric");
    }

    @Test
    public void testKeysCannotStartWithUnderscore() {
        try {
            from("{_foo}");
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "Unexpected first character '_'; must be lower case a-z.");
        }
    }

    @Test
    public void testRetainsSpans() {
        StringBuilder ssb =
                new StringBuilder("Hello {name}, you are {age} years old.");

        CharSequence formatted = from(ssb).put("name",
                "Abe").put("age", 20).format();
        Assert.assertEquals(formatted.toString(), "Hello Abe, you are 20 years old.");
    }

    @Test
    public void testIntoSetsTargetText() {
        final IAbilityDelegator abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        final Context context = abilityDelegator.getCurrentTopAbility().getContext();
        Text textView = new Text(context);
        from("Hello {user}!").put("user", "Eric").into(textView);
        CharSequence actual = textView.getText();
        org.junit.Assert.assertEquals(actual.toString(), "Hello Eric!");
    }

    @Test
    public void testIntoNullFailsFast() {
        try {
            from("Hello {user}!").put("user", "Eric").into(null);
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "Text must not be null.");
        }
    }

}
