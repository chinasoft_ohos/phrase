package com.squareup.longaike.slice;

import com.squareup.longaike.ResourceTable;
import com.squareup.phrase.ListPhrase;
import com.squareup.phrase.Phrase;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.Arrays;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Text text1 = (Text) findComponentById(ResourceTable.Id_text1);
        Text text2 = (Text) findComponentById(ResourceTable.Id_text2);
        Text text3 = (Text) findComponentById(ResourceTable.Id_text3);
        Text text4 = (Text) findComponentById(ResourceTable.Id_text4);

        text4.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ListPhrase from = ListPhrase.from("+");
                CharSequence sequence2 = from.join(Arrays.asList("one", "two"));

                text2.setText(sequence2.toString());
            }
        });

        text3.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                CharSequence format = Phrase.from("Hi {first_name}, you are {age} years old.")
                        .put("first_name", "UperOne")
                        .put("age", "26")
                        .format();
                text2.setText(format.toString());
            }
        });

        text1.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {

                    Phrase.from(text2, ResourceTable.String_user_name).into(text2);

                } catch (IOException | NotExistException | WrongTypeException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
