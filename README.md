#  phrase

#### 项目介绍
- 项目名称：phrase
- 所属系列：openharmony的第三方组件适配移植
- 功能：资源化字符串
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Release  1.1.0

#### 效果演示

![screen1](https://images.gitee.com/uploads/images/2021/0528/173016_803b9a70_8946212.gif "screen1.gif")

![screen2](https://images.gitee.com/uploads/images/2021/0528/173144_a2d2d869_8946212.gif "screen2.gif")

![screen3](https://images.gitee.com/uploads/images/2021/0528/173232_14966885_8946212.gif "screen3.gif")


#### 安装教程

1.在项目根目录下的build.gradle文件中，

 ```

allprojects {

    repositories {

        maven {

            url 'https://s01.oss.sonatype.org/content/repositories/releases/'

        }

    }

}

 ```

2.在entry模块的build.gradle文件中，

 ```

 dependencies {

    implementation('com.gitee.chinasoft_ohos:phrase:1.1.1')

    ......  

 }

 ```
在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1.将仓库导入到本地仓库中

2.在布局文件中加入Text控件,代码实例如下:
```
  <Text
                ohos:id="$+id:text1"
                ohos:height="match_content"
                ohos:width="match_content"
                ohos:auto_font_size="false"
                ohos:background_element="$graphic:bg_rounded_rectangle_blue"
                ohos:bottom_margin="15vp"
                ohos:italic="true"
                ohos:left_padding="8vp"
                ohos:max_text_lines="1"
                ohos:min_height="30vp"
                ohos:multiple_lines="true"
                ohos:right_padding="8vp"
                ohos:text="获取user_name"
                ohos:text_color="#0000FF"
                ohos:text_font="serif"
                ohos:text_size="30fp"
                />
```
3.在AbilitySlice的onStart方法中获取控件,并设置文本内容

方式一:
```
       ListPhrase from = ListPhrase.from("+");
       CharSequence sequence2 = from.join(Arrays.asList("one", "two"));  
       text1.setText(sequence2.toString());
```
方式二:
```
        CharSequence format = Phrase.from("Hi {first_name}, you are {age} years old.")
                        .put("first_name", "UperOne")
                        .put("age", "26")
                        .format();
        text1.setText(format.toString());
```
方式三:
```
        Phrase.from(text1, ResourceTable.String_user_name).into(text1);
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.1.1
- 0.0.1-SNAPSHOT

### 版权和许可信息

```
Copyright 2013 Square, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```