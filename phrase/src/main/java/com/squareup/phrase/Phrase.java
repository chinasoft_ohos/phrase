/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.squareup.phrase;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A fluent API for formatting Strings. Canonical usage:
 * <pre>
 *   CharSequence formatted = Phrase.from("Hi {first_name}, you are {age} years old.")
 *       .put("first_name", firstName)
 *       .put("age", age)
 *       .format();
 * </pre>
 * <ul>
 * <li>Surround keys with curly braces; use two {{ to escape.</li>
 * <li>Keys start with lowercase letters followed by lowercase letters and underscores.</li>
 * <li>Spans are preserved, such as simple HTML tags found in strings.xml.</li>
 * <li>Fails fast on any mismatched keys.</li>
 * </ul>
 * The constructor parses the original pattern into a doubly-linked list of {@link Token}s.
 * These tokens do not modify the original pattern, thus preserving any spans.
 * <p>
 * The {@link #format()} method iterates over the tokens, replacing text as it iterates. The
 * doubly-linked list allows each token to ask its predecessor for the expanded length.
 */
public final class Phrase {

    /**
     * The unmodified original pattern.
     */
    private final CharSequence pattern;

    /**
     * All keys parsed from the original pattern, sans braces.
     */
    private final Set<String> keys = new HashSet<String>();
    private final Map<String, CharSequence> keysToValues = new HashMap<String, CharSequence>();

    /**
     * Cached result after replacing all keys with corresponding values.
     */
    private CharSequence formatted;

    /**
     * The constructor parses the original pattern into this doubly-linked list of tokens.
     */
    private Token head;

    /**
     * When parsing, this is the current character.
     */
    private char curChar;
    private int curCharIndex;

    /**
     * Indicates parsing is complete.
     */
    private static final int EOF = 0;

    /**
     * Entry point into this API.
     *
     * @param f                 FractionAbility
     * @param patternResourceId patternResourceId
     * @return Phrase phrase
     * @throws NotExistException  NotExistException.
     * @throws WrongTypeException WrongTypeException.
     * @throws IOException        IOException.
     */
    public static Phrase from(FractionAbility f, int patternResourceId) throws NotExistException, WrongTypeException, IOException {
        return from(f.getResourceManager().getElement(patternResourceId).getString());
    }

    /**
     * Entry point into this API.
     *
     * @param v                 Component
     * @param patternResourceId patternResourceId
     * @return Phrase Phrase
     * @throws NotExistException  if pattern contains any syntax errors.
     * @throws WrongTypeException if pattern contains any syntax errors.
     * @throws IOException        if pattern contains any syntax errors.
     */
    public static Phrase from(Component v, int patternResourceId) throws NotExistException, WrongTypeException, IOException {
        return from(v.getResourceManager().getElement(patternResourceId).getString());
    }

    /**
     * Entry point into this API.
     *
     * @param c                 Context
     * @param patternResourceId patternResourceId
     * @return Phrase Phrase
     * @throws NotExistException  if pattern contains any syntax errors.
     * @throws WrongTypeException if pattern contains any syntax errors.
     * @throws IOException        if pattern contains any syntax errors.
     */
    public static Phrase from(Context c, int patternResourceId) throws NotExistException, WrongTypeException, IOException {
        return from(c.getResourceManager().getElement(patternResourceId).getString());
    }

    /**
     * Entry point into this API.
     * *
     *
     * @param r                 ResourceManager
     * @param patternResourceId patternResourceId
     * @return Phrase Phrase
     * @throws NotExistException  if pattern contains any syntax errors.
     * @throws WrongTypeException if pattern contains any syntax errors.
     * @throws IOException        if pattern contains any syntax errors.
     */
    public static Phrase from(ResourceManager r, int patternResourceId) throws NotExistException, WrongTypeException, IOException {
        return from(r.getElement(patternResourceId).getString());
    }

    /**
     * Entry point into this API.
     *
     * @param v                 Component
     * @param patternResourceId patternResourceId
     * @param quantity          quantity
     * @return Phrase Phrase
     * @throws NotExistException  if pattern contains any syntax errors.
     * @throws WrongTypeException if pattern contains any syntax errors.
     * @throws IOException        if pattern contains any syntax errors.
     */
    public static Phrase fromPlural(Component v, int patternResourceId, int quantity) throws NotExistException, WrongTypeException, IOException {
        return fromPlural(v.getContext(), patternResourceId, quantity);
    }

    /**
     * Entry point into this API.
     *
     * @param c                 Context
     * @param patternResourceId patternResourceId
     * @param quantity          quantity
     * @return Phrase Phrase
     * @throws NotExistException  if pattern contains any syntax errors.
     * @throws WrongTypeException if pattern contains any syntax errors.
     * @throws IOException        if pattern contains any syntax errors.
     */
    public static Phrase fromPlural(Context c, int patternResourceId, int quantity) throws NotExistException, WrongTypeException, IOException {
        return fromPlural(c.getResourceManager(), patternResourceId, quantity);
    }

    /**
     * Entry point into this API.
     *
     * @param r                 ResourceManager
     * @param patternResourceId patternResourceId
     * @param quantity          quantity
     * @return Phrase Phrase
     * @throws NotExistException  if pattern contains any syntax errors.
     * @throws WrongTypeException if pattern contains any syntax errors.
     * @throws IOException        if pattern contains any syntax errors.
     */
    public static Phrase fromPlural(ResourceManager r, int patternResourceId, int quantity) throws NotExistException, WrongTypeException, IOException {
        return from(r.getElement(patternResourceId).getPluralString(quantity));
    }

    /**
     * Entry point into this API; pattern must be non-null.
     *
     * @param pattern CharSequence
     * @return Phrase Phrase
     */
    public static Phrase from(CharSequence pattern) {
        return new Phrase(pattern);
    }

    /**
     * Replaces the given key with a non-null value. You may reuse Phrase instances and replace
     * keys with new values.
     *
     * @param key   key
     * @param value value
     * @return Phrase Phrase
     * @throws IllegalArgumentException if the key is not in the pattern.
     */
    public Phrase put(String key, CharSequence value) {
        if (!keys.contains(key)) {
            throw new IllegalArgumentException("Invalid key: " + key);
        }
        if (value == null) {
            throw new IllegalArgumentException("Null value for '" + key + "'");
        }
        keysToValues.put(key, value);

        // Invalidate the cached formatted text.
        formatted = null;
        return this;
    }

    /**
     * Replaces the given key with the {@link Integer#toString(int)} value for the given int.
     *
     * @param key   key
     * @param value value
     * @return Phrase phrase
     * @see #put(String, CharSequence)
     */
    public Phrase put(String key, int value) {
        return put(key, Integer.toString(value));
    }

    /**
     * Silently ignored if the key is not in the pattern.
     *
     * @param key   key
     * @param value value
     * @return Phrase phrase
     * @see #put(String, CharSequence)
     */
    public Phrase putOptional(String key, CharSequence value) {
        return keys.contains(key) ? put(key, value) : this;
    }

    /**
     * Replaces the given key, if it exists, with the {@link Integer#toString(int)} value
     * for the given int.
     *
     * @param key   key
     * @param value value
     * @return Phrase phrase
     * @see #putOptional(String, CharSequence)
     */
    public Phrase putOptional(String key, int value) {
        return keys.contains(key) ? put(key, value) : this;
    }

    /**
     * Returns the text after replacing all keys with values.
     *
     * @return the text after replacing all keys with values
     * @throws IllegalArgumentException if any keys are not replaced.
     */
    public CharSequence format() {
        if (formatted == null) {
            if (!keysToValues.keySet().containsAll(keys)) {
                Set<String> missingKeys = new HashSet<>(keys);
                missingKeys.removeAll(keysToValues.keySet());
                throw new IllegalArgumentException("Missing keys: " + missingKeys);
            }

            // Copy the original pattern to preserve all spans, such as bold, italic, etc.
            StringBuilder sb = new StringBuilder(pattern);
            for (Token t = head; t != null; t = t.next) {
                t.expand(sb, keysToValues);
            }

            formatted = sb;
        }
        return formatted;
    }

    /**
     * "Formats and sets as text in textView."
     *
     * @param textView Text
     * @throws IllegalArgumentException
     */
    public void into(Text textView) {
        if (textView == null) {
            throw new IllegalArgumentException("Text must not be null.");
        }
        textView.setText(format().toString());
    }

    /**
     * toString()
     * through to {@link #format()} because doing so would drop all spans.
     *
     * @return Returns the raw pattern without expanding keys; only useful for debugging. Does not pass
     */
    @Override
    public String toString() {
        return pattern.toString();
    }

    private Phrase(CharSequence pattern) {
        curChar = (pattern.length() > 0) ? pattern.charAt(0) : EOF;

        this.pattern = pattern;

        // A hand-coded lexer based on the idioms in "Building Recognizers By Hand".
        // http://www.antlr2.org/book/byhand.pdf.
        Token prev = null;
        Token next;
        while ((next = token(prev)) != null) {
            // Creates a doubly-linked list of tokens starting with head.
            if (head == null) head = next;
            prev = next;
        }
    }

    /**
     * token
     *
     * @param prev Token
     *             Returns the next token from the input pattern, or null when finished parsing.
     * @return Returns the next token from the input pattern, or null when finished parsing.
     * @throws IllegalArgumentException Exception
     */
    private Token token(Token prev) {
        if (curChar == EOF) {
            return null;
        }
        if (curChar == '{') {
            char nextChar = lookahead();
            if (nextChar == '{') {
                return leftCurlyBracket(prev);
            } else if (nextChar >= 'a' && nextChar <= 'z') {
                return key(prev);
            } else {
                throw new IllegalArgumentException(
                        "Unexpected first character '" + nextChar + "'; must be lower case a-z.");
            }
        }
        return text(prev);
    }

    /**
     * key
     *
     * @param prev Token
     * @return KeyToken KeyToken
     * @throws IllegalArgumentException IllegalArgumentException
     *                                  Parses a key: "{some_key}".
     */
    private KeyToken key(Token prev) {

        // Store keys as normal Strings; we don't want keys to contain spans.
        StringBuilder sb = new StringBuilder();

        // Consume the opening '{'.
        consume();
        while ((curChar >= 'a' && curChar <= 'z') || curChar == '_') {
            sb.append(curChar);
            consume();
        }

        // Consume the closing '}'.
        if (curChar != '}') {
            throw new IllegalArgumentException("Unexpected character '" + curChar
                    + "'; expecting lower case a-z, '_', or '}'");
        }
        consume();

        // Disallow empty keys: {}.
        if (sb.length() == 0) {
            throw new IllegalArgumentException("Empty key: {}");
        }

        String key = sb.toString();
        keys.add(key);
        return new KeyToken(prev, key);
    }

    /**
     * Consumes and returns a token for a sequence of text.
     *
     * @param prev Token
     * @return KeyToken KeyToken
     */
    private TextToken text(Token prev) {
        int startIndex = curCharIndex;

        while (curChar != '{' && curChar != EOF) {
            consume();
        }
        return new TextToken(prev, curCharIndex - startIndex);
    }

    /**
     * Consumes and returns a token representing two consecutive curly brackets.
     *
     * @param prev Token
     * @return LeftCurlyBracketToken LeftCurlyBracketToken
     */
    private LeftCurlyBracketToken leftCurlyBracket(Token prev) {
        consume();
        consume();
        return new LeftCurlyBracketToken(prev);
    }

    /**
     * Returns the next character in the input pattern without advancing.
     *
     * @return Returns the next character in the input pattern without advancing.
     */
    private char lookahead() {
        return curCharIndex < pattern.length() - 1 ? pattern.charAt(curCharIndex + 1) : EOF;
    }

    /**
     * Advances the current character position without any error checking. Consuming beyond the
     * end of the string can only happen if this parser contains a bug.
     */
    private void consume() {
        curCharIndex++;
        curChar = (curCharIndex == pattern.length()) ? EOF : pattern.charAt(curCharIndex);
    }

    private abstract static class Token {
        private final Token prev;
        private Token next;

        protected Token(Token prev) {
            this.prev = prev;
            if (prev != null) prev.next = this;
        }

        /**
         * Replace text in {@code target} with this token's associated value.
         *
         * @param target StringBuilder
         * @param data   data
         */
        abstract void expand(StringBuilder target, Map<String, CharSequence> data);

        /**
         * Returns the number of characters after expansion.
         *
         * @return Returns the number of characters after expansion.
         */
        abstract int getFormattedLength();

        /**
         * Returns the character index after expansion.
         *
         * @return Returns the character index after expansion.
         */
        final int getFormattedStart() {
            if (prev == null) {
                // The first token.
                return 0;
            } else {
                // Recursively ask the predecessor node for the starting index.
                return prev.getFormattedStart() + prev.getFormattedLength();
            }
        }
    }

    /**
     * Ordinary text between tokens.
     */
    private static class TextToken extends Token {
        private final int textLength;

        TextToken(Token prev, int textLength) {
            super(prev);
            this.textLength = textLength;
        }

        @Override
        void expand(StringBuilder target, Map<String, CharSequence> data) {
            // Don't alter spans in the target.
        }

        @Override
        int getFormattedLength() {
            return textLength;
        }
    }

    /**
     * A sequence of two curly brackets.
     */
    private static class LeftCurlyBracketToken extends Token {
        LeftCurlyBracketToken(Token prev) {
            super(prev);
        }

        @Override
        void expand(StringBuilder target, Map<String, CharSequence> data) {
            int start = getFormattedStart();
            target.replace(start, start + 2, "{");
        }

        @Override
        int getFormattedLength() {
            // Replace {{ with {.
            return 1;
        }
    }

    private static class KeyToken extends Token {
        /**
         * The key without { and }.
         */
        private final String key;

        private CharSequence value;

        KeyToken(Token prev, String key) {
            super(prev);
            this.key = key;
        }

        @Override
        void expand(StringBuilder target, Map<String, CharSequence> data) {
            value = data.get(key);
            int replaceFrom = getFormattedStart();
            // Add 2 to account for the opening and closing brackets.
            int replaceTo = replaceFrom + key.length() + 2;
            target.replace(replaceFrom, replaceTo, value.toString());
        }

        @Override
        int getFormattedLength() {
            // Note that value is only present after expand. Don't error check because this is all
            // private code.
            return value.length();
        }
    }
}
